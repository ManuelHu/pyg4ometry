FROM ubuntu:jammy

RUN mkdir -p /run/systemd && echo 'docker' > /run/systemd/container

CMD ["/bin/bash"]

WORKDIR /tmp

# start with more uptodate packages
RUN apt-get update

# build essentials 
RUN apt -y install build-essential

# required packages
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata libxt-dev libglx-dev libgl1-mesa-dev \
    wget libboost-all-dev emacs xvfb x11vnc fvwm libcgal-dev git libmpfr-dev libgmp-dev pybind11-dev

# python 
RUN apt-get -y install python3 python3-pip && \
    pip3 install cython ipython pybind11 pandas distro

# vtk 
RUN wget https://www.vtk.org/files/release/9.2/vtk-9.2.0rc1-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl && \
    pip3 install vtk-9.2.0rc1-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl

# freecad
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install freecad && \
    ln -s /usr/lib/freecad/Mod /usr/lib/freecad-python3/&& \
    export PYTHONPATH=/usr/lib/freecad/lib/

# default python
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1 && \
    update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1

# pyg4ometry
ARG PYG4_TAG=1
RUN git clone https://stewartboogert@bitbucket.org/jairhul/pyg4ometry.git && \
    cd pyg4ometry &&\
    git checkout develop
    
RUN cd pyg4ometry && \
    export export SETUPTOOLS_USE_DISTUTILS=stdlib && \
    pip install --editable . --user    

