==========
Authorship
==========

The following people have contributed to pyfluka:

* Stuart Walker
* Laurie Nevay
* Andrey Abramov
* Stewart Boogert
