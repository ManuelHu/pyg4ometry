============
Installation
============


Requirements
------------

 * python2.7
 * pygdml


Installation
------------

::
   git clone http://bitbucket.org/jairhul/pyfluka
   cd pyfluka
   make install
