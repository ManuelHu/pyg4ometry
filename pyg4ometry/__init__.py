from . import config
from . import compare
from . import convert
from . import exceptions
from . import fluka
from . import freecad
from . import gdml
from . import io
from . import geant4
from . import pycgal
from . import pyoce
#try:
#    from . import gui
#except ImportError:
#    import warnings
#    warnings.warn("Failed to import pyg4ometry.gui subpackage.")
#    del warnings
from . import stl
from . import test
from . import transformation
from . import visualisation
from . import features
from . import bdsim

__version__ = "1.0.1"
